﻿using ClientApp.MVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ClientApp.MVVM.Views;

namespace Client
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		public static MainWindowViewModel MainWindowViewModel { get; set; }

		/// <summary>
		/// Обработка запуска приложения.
		/// </summary>
		private void ApplicationOnStartup(object sender, StartupEventArgs e)
		{
			try
			{
				StartApp();
			}
			catch (Exception exception)
			{
				MessageBox.Show(exception.ToString());
				throw;
			}
		}

		/// <summary>
		/// Запуск приложения.
		/// </summary>
		private void StartApp()
		{
			// Старт основного окна установки.
			MainWindowViewModel = new MainWindowViewModel();
			var mainWindow = new MainWindowView() { DataContext = MainWindowViewModel };
			MainWindow = mainWindow;
			MainWindow.Show();
		}
	}
}
