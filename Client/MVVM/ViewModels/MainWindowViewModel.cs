﻿using System;
using System.Net.NetworkInformation;
using System.Windows;
using System.Windows.Input;
using ClientApp.Common;
using ClientServerLib.Floods;

namespace ClientApp.MVVM.ViewModels
{
	public class MainWindowViewModel : DependencyObject
	{
		public LightBot LightBot { get; set; }

		public PingFloodBot PingFloodBot { get; set; }

		public string IpAddress
		{
			get => (string)GetValue(IpAddressProperty);
			set => SetValue(IpAddressProperty, value);
		}

		public static readonly DependencyProperty IpAddressProperty =
			DependencyProperty.Register(nameof(IpAddress), typeof(string), typeof(MainWindowViewModel), new PropertyMetadata("192.168.0.0"));


		public int Port
		{
			get => (int)GetValue(PortProperty);
			set => SetValue(PortProperty, value);
		}

		public static readonly DependencyProperty PortProperty =
			DependencyProperty.Register(nameof(Port), typeof(int), typeof(MainWindowViewModel), new PropertyMetadata(11000));
		

		public bool AttackIsEnable
		{
			get => (bool)GetValue(AttackIsEnableProperty);
			set => SetValue(AttackIsEnableProperty, value);
		}

		public static readonly DependencyProperty AttackIsEnableProperty =
			DependencyProperty.Register(nameof(AttackIsEnable), typeof(bool), typeof(MainWindowViewModel), new PropertyMetadata(false));

		public int PerThread
		{
			get => (int)GetValue(PerThreadProperty);
			set => SetValue(PerThreadProperty, value);
		}

		public static readonly DependencyProperty PerThreadProperty =
			DependencyProperty.Register(nameof(PerThread), typeof(int), typeof(MainWindowViewModel), new PropertyMetadata(4));


		public string ConsoleText
		{
			get => (string)GetValue(ConsoleTextProperty);
			set => SetValue(ConsoleTextProperty, value);
		}

		public static readonly DependencyProperty ConsoleTextProperty =
			DependencyProperty.Register(nameof(ConsoleText), typeof(string), typeof(MainWindowViewModel), new PropertyMetadata(string.Empty));

		public ICommand StartOrStopCommand { get; set; }

		public ICommand StartOrStopPingCommand { get; set; }

		public MainWindowViewModel()
		{
			LightBot = new LightBot();
			PingFloodBot = new PingFloodBot();

			StartOrStopCommand = new RelayCommand(o =>
			{
				if (!LightBot.IsEnabled)
				{
					LightBot.IsEnabled = true;
					LightBot.IpAddress = IpAddress;
					LightBot.Port = Port;
					LightBot.Callback = ar => { ConsoleText += ar.ToString() + Environment.NewLine; };
					LightBot.StartBotAttack();
				}
				else
				{
					LightBot.IsEnabled = false;
				}
			});

			StartOrStopPingCommand = new RelayCommand(o => {
				if (!PingFloodBot.IsEnabled) {
					PingFloodBot.IsEnabled = true;
					PingFloodBot.IpAddress = IpAddress;
					PingFloodBot.StartBotAttack();
				}
				else
				{
					PingFloodBot.IsEnabled = false;
				}
			});
		}
	}
}
