using System;
using System.Net;
using ClientServerLib.Client;
using ClientServerLib.Common;
using ClientServerLib.Floods;

namespace ClientConsoleApp
{
	class Program
	{
		static void Main(string[] args)
		{
			StartApp();
		}

		private static void StartApp()
		{
			try {
				StartLightBot();

				//StartClient();
				//StartSyn();
				//AsynchronousClient.StartClient();

			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
			finally
			{
				Console.ReadLine();
			}
		}

		private static void StartClient()
		{
			var ip = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0];
			while (true)
			{
				Console.Write("Введите сообщение: ");
				var message = Console.ReadLine();
				var clientStateObj = new ClientStateObject(ip, 11000);

				ClientService.SendMessageFromSocket(clientStateObj, message);
			}
		}

		private static void StartBotClient()
		{
			var message = "170";
			var ip = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0];
			while (true) {
				var clientStateObj = new ClientStateObject(ip, 11000);
				ClientService.SendMessageFromSocket(clientStateObj, message);
			}
		}

		private static void StartSyn()
		{
			IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
			IPAddress ipAddress = ipHostInfo.AddressList[0];
			Syn.Host = new IPEndPoint(ipAddress, 11000);
			Syn.IsEnabled = true;
			Syn.socketsPerThread = 1;
			Syn.callback = ar => { Console.WriteLine(ar.ToString()); };
			Syn.WorkerThread();
		}

		private static void StartLightBot()
		{
			//LightBot.Instance.
			//IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
			////IPAddress ipAddress = ipHostInfo.AddressList[0];
			////IPAddress ipAddress = System.Net.IPAddress.Parse("192.168.0.102");
			////IPAddress ipAddress = ipHostInfo.AddressList[0];
			//IPAddress ipAddress = System.Net.IPAddress.Parse("192.168.0.101");
			//LightBot.Host = new IPEndPoint(ipAddress, 11000);
			//LightBot.IsEnabled = true;
			//LightBot.socketsPerThread = 1;
			//LightBot.callback = ar => { Console.WriteLine(ar.ToString()); };
			//LightBot.StartBotAttack();
		}
	}
}
