﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using ClientServerLib.Common;

namespace ClientServerLib.Client
{
	public class ClientService
	{
		public static void SendMessageFromSocket(ClientStateObject clientStateObject, string message)
		{
			// Соединяемся с удаленным устройством
			//clientStateObject.WorkSocket = new Socket(clientStateObject.ClientIpAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

			var sender = clientStateObject.WorkSocket;
			//var sender = GetSender(clientStateObject);

			// Соединяем сокет с удаленной точкой
			sender.Connect(clientStateObject.IpEndPoint);

			Console.WriteLine($"Сокет соединяется с {sender.RemoteEndPoint} ");
			var msg = Encoding.UTF8.GetBytes(message);

			// Отправляем данные через сокет
			var bytesSent = sender.Send(msg);

			// Получаем ответ от сервера
			var bytesRec = sender.Receive(clientStateObject.Buffer);

			Console.WriteLine("\nОтвет от сервера: {0}\n\n", Encoding.UTF8.GetString(clientStateObject.Buffer, 0, bytesRec));

			//clientStateObject.Buffer = new byte[1024];

			// Освобождаем сокет
			sender.Shutdown(SocketShutdown.Both);
			sender.Close();
		}

		public static void SendMessageFromSocket(ClientStateObject clientStateObject, byte[] message)
		{
			// Соединяемся с удаленным устройством
			//clientStateObject.WorkSocket = new Socket(clientStateObject.ClientIpAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

			var sender = clientStateObject.WorkSocket;
			//var sender = GetSender(clientStateObject);

			// Соединяем сокет с удаленной точкой
			sender.Connect(clientStateObject.IpEndPoint);

			Console.WriteLine($"Сокет соединяется с {sender.RemoteEndPoint} ");

			// Отправляем данные через сокет
			var bytesSent = sender.Send(message);

			// Получаем ответ от сервера
			var bytesRec = sender.Receive(clientStateObject.Buffer);

			Console.WriteLine("\nОтвет от сервера: {0}\n\n", Encoding.UTF8.GetString(clientStateObject.Buffer, 0, bytesRec));

			//clientStateObject.Buffer = new byte[1024];

			// Освобождаем сокет
			sender.Shutdown(SocketShutdown.Both);
			sender.Close();
		}
	}
}
