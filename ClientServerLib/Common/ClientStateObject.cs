﻿using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ClientServerLib.Common
{
	/// <summary>
	/// State object for client.
	/// </summary>
	public class ClientStateObject : StateObject
	{
		public  IPAddress ClientIpAddress { get; } = Dns.GetHostEntry("localhost").AddressList[0];

		public IPAddress ServerIpAddress { get; }

		public IPEndPoint IpEndPoint { get; set; }

		public int Port { get; }

		public ClientStateObject(IPAddress ipAddress, int port)
		{
			ServerIpAddress = ipAddress;
			Port = port;
			IpEndPoint = new IPEndPoint(ServerIpAddress, Port);
			WorkSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		}
	}
}
