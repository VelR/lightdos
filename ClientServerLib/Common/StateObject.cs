﻿using System.Net.Sockets;
using System.Text;

namespace ClientServerLib.Common
{
	/// <summary>
	/// State object for reading client data asynchronously.
	/// (С) exaple: https://docs.microsoft.com/ru-ru/dotnet/framework/network-programming/asynchronous-server-socket-example
	/// </summary>
	public class StateObject
	{
		/// <summary>
		/// Size of receive buffer.  
		/// </summary>
		public const int BufferSize = 1024;

		/// <summary>
		/// Client  socket.
		/// </summary>
		public Socket WorkSocket { get; set; } = null;

		/// <summary>
		/// Receive buffer.  
		/// </summary>
		public byte[] Buffer { get; } = new byte[BufferSize];

		/// <summary>
		/// Received data string.  
		/// </summary>
		public StringBuilder StringBuilder { get; } = new StringBuilder();
	}
}
