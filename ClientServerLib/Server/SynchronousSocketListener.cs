﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ClientServerLib.Server
{
	public class SynchronousSocketListener
	{
		public static Func<string, string> ActionWithMessage;
		public static List<IPAddress> BlackList = new List<IPAddress>();
		public static Dictionary<IPAddress, int> IpAddressCounter = new Dictionary<IPAddress, int>();
		public static Timer Timer;

		private static void InitTimer()
		{
			Timer = new Timer(
				callback: TimerTask,
				state: 0,
				dueTime: 1000,
				period: 2000);
		}
		private static void TimerTask(object timerState)
		{
			lock (IpAddressCounter)
			{
				IpAddressCounter.Clear();
			}
		}

		public static void StartListening(bool isGuard = false)
		{
			IPHostEntry ipHostInfo = Dns.GetHostEntry("localhost");
			IPAddress ipAddress = ipHostInfo.AddressList[0];
			IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, 11000);

			if (isGuard)
			{
				Console.WriteLine("Включен защищенный режим");
				InitTimer();
			}

			// Create a TCP/IP socket.  
			Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

			// Bind the socket to the local endpoint and listen for incoming connections.  
			try
			{
				listener.Bind(localEndPoint);
				listener.Listen(100);

				// Начинаем слушать соединения
				while (true)
				{
					Console.WriteLine("Ожидаем соединение через порт {0}", localEndPoint);

					// Программа приостанавливается, ожидая входящее соединение
					Socket handler = listener.Accept();
					string data = null;

					Console.WriteLine($"Соединение с {(handler.LocalEndPoint as IPEndPoint).Address}");

					// Мы дождались клиента, пытающегося с нами соединиться

					if (isGuard && !VerifyIpAddress((handler.LocalEndPoint as IPEndPoint).Address))
					{
						Console.WriteLine($"Соединение с {(handler.LocalEndPoint as IPEndPoint).Address} заблокировано, разрыв соединения.");
						handler.Shutdown(SocketShutdown.Both);
						handler.Close();
						continue;
					}

					byte[] bytes = new byte[1024];
					int bytesRec = handler.Receive(bytes);

					data += Encoding.UTF8.GetString(bytes, 0, bytesRec);

					// Показываем данные на консоли
					Console.Write("Полученный текст: " + data + "\n\n");

					string reply;

					if (ActionWithMessage != null)
					{
						reply = ActionWithMessage.Invoke(data);
					}
					else
					{
						reply = $"Спасибо за запрос в {data.Length} символов";
					}

					// Отправляем ответ клиенту\
					byte[] msg = Encoding.UTF8.GetBytes(reply);
					handler.Send(msg);

					if (data.IndexOf("<TheEnd>") > -1)
					{
						Console.WriteLine("Сервер завершил соединение с клиентом.");
						break;
					}

					handler.Shutdown(SocketShutdown.Both);
					handler.Close();
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
			finally
			{

			}

			Console.WriteLine("\nPress ENTER to continue...");
			Console.Read();
		}

		private static bool VerifyIpAddress(IPAddress ipAddress)
		{
			if (!BlackList.Contains(ipAddress))
			{

				if (!IpAddressCounter.ContainsKey(ipAddress))
				{
					IpAddressCounter.Add(ipAddress, 0);
				}

				IpAddressCounter[ipAddress] += 1;

				if (IpAddressCounter[ipAddress] >= 15)
				{
					BlackList.Add(ipAddress);
				}
			}


			return !BlackList.Contains(ipAddress);
		}
	}
}
