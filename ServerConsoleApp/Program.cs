using System;
using ClientServerLib.Server;

namespace ServerConsoleApp
{
	class Program
	{
		static void Main(string[] args)
		{
			StartApp(args);
		}

		private static void StartApp(string[] args)
		{
			while (true) {
				try {
					SynchronousSocketListener.ActionWithMessage = GetReplyMessage;
					SynchronousSocketListener.StartListening(args.Length != 0);
					//AsynchronousSocketListener.StartListening();
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
					throw;
				}
			}
		}

		private static string GetReplyMessage(string data)
		{
			string reply;
			// Отправляем ответ клиенту\
			if (int.TryParse(data, out var factorialResult))
			{
				if (factorialResult > 170)
				{
					reply = $"факториал числа {data} не может быть вычислен";
				}
				else
				{
					reply = $"факториал числа {data}: {GetFactorial(factorialResult)}";
				}
			}
			else
			{
				reply = $"Запрос {data} не возможно преобразовать в число:";
			}

			return reply;
		}

		static double GetFactorial(int number)
		{
			return number <= 1 ? 1 : number * GetFactorial(number - 1);
		}
	}
}
